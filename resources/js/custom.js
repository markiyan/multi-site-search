﻿
$('#searchTerm').on('keypress', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        alert("Please select which site to search from the menu on the left.")
    }
});

function resizeIframe() {
    var height = document.documentElement.clientHeight;
    height -= document.getElementById('frame').offsetTop;
    // not sure how to get this dynamically
    height -= 20; /* whatever you set your body bottom margin/padding to be */
    document.getElementById('frame').style.height = height + "px";
};

document.getElementById('frame').onload = resizeIframe;
window.onresize = resizeIframe;
    
$(document).ready(function (frameURL) {
    $("#btnBing").click(function () {
        $('#frame').attr('src', "http://www.bing.com/search?q=" + $('#searchTerm').val())
    });
    $("#btnGoGoDuck").click(function () {
        $('#frame').attr('src', "https://duckduckgo.com/?q=" + $('#searchTerm').val())
    });
    $("#btnAsk").click(function () {
        $('#frame').attr('src', "http://www.ask.com/web?q=" + $('#searchTerm').val())
    });
    $("#btnBBB").click(function () {
        $('#frame').attr('src', "http://www.bbb.org/search/?term=" + $('#searchTerm').val())
    });
    $("#btnJayde").click(function () {
        $('#frame').attr('src', "http://www.jayde.com/schx.html?q=" + $('#searchTerm').val())
    });
    $("#btnKompass").click(function () {
        $('#frame').attr('src', "http://us.kompass.com/searchCompanies?searchType=ALL&text=" + $('#searchTerm').val())
    });
    $("#btnArchive").click(function () {
        $('#frame').attr('src', "https://archive.org/search.php?query=" + $('#searchTerm').val())
    });
    $("#btnYippy").click(function () {
        $('#frame').attr('src', "http://clusty.com/search?query=" + $('#searchTerm').val())
    });
    $("#btnDogpile").click(function () {
        $('#frame').attr('src', "http://www.dogpile.com/info.dogpl.t12.3/search/web?q=" + $('#searchTerm').val())
    });
    $("#btnYellowPages").click(function () {
        $('#frame').attr('src', "http://www.yellowpages.com/search?search_terms=" + $('#searchTerm').val() + "&geo_location_terms=USA")
    });
    $("#btnRipoffReport").click(function () {
        $('#frame').attr('src', "http://www.ripoffreport.com/reports/specific_search/" + $('#searchTerm').val())
    });
    $("#btnMelissaData").click(function () {
        $('#frame').attr('src', "http://www.melissadata.com/lookups/iplocation.asp?ipaddress=" + $('#searchTerm').val())
    });
    $("#btnInfoSniper").click(function () {
        $('#frame').attr('src', "http://www.infosniper.net/index.php?ip_address=" + $('#searchTerm').val())
    });
    $("#btnWikipedia").click(function () {
        $('#frame').attr('src', "https://en.wikipedia.org/w/index.php?search=" + $('#searchTerm').val())
    });

    $("#btnRubMaps").click(function () {
        window.open("http://www.rubmaps.com/search-" + $('#searchTerm').val(), '');
    });
    $("#btnPipl").click(function () {
        window.open("https://pipl.com/search/?q=" + $('#searchTerm').val(), '');
    });
    $("#btnYandex").click(function () {
        window.open("https://www.yandex.com/yandsearch?text=" + $('#searchTerm').val(), '');
    });
    $("#btnYahoo").click(function () {
        window.open("https://search.yahoo.com/search?p=" + $('#searchTerm').val(), '');
    });
    $("#btnGoogle").click(function () {
        window.open("https://www.google.com/search?q=" + $('#searchTerm').val(), '');
    });
    $("#btnFacebook").click(function () {
        window.open("https://www.facebook.com/search/more?q=" + $('#searchTerm').val(), '');
    });
    $("#btnCrunchBase").click(function () {
        window.open("http://crunchbase.com/search?query=" + $('#searchTerm').val(), '');
    });
});