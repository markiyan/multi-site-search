# README #

You can see a live demo at: http://mm-search.azurewebsites.net

### What is this repository for? ###

* Simple web page that lets the user enter a search term and look it up on various search sites like google and some specific sites (ex: IP address lookup).
* Version: 1.0

### How do I get set up? ###

* This is just HTML/CSS/JS.